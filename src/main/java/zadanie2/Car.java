package zadanie2;

public class Car implements IVehicle {
    private String name;

    public Car(String name) {
        this.name = name;
    }

    @Override
    public void moveRight() {
        System.out.println("Przesuwam " + name + " w prawo");
    }

    @Override
    public void moveLeft() {
        System.out.println("Przesuwam " + name + " w lewo");

    }

    @Override
    public void moveUp() {
        System.out.println("Przesuwam " + name + " w górę");

    }

    @Override
    public void moveDown() {
        System.out.println("Przesuwam " + name + " w dół");

    }
}
