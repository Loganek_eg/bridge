package zadanie1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;


public class MyFilePrinter implements IPrinter {
    private File file = new File("C:\\Users\\Logan\\Projekty\\Bridge\\src\\main\\java\\zadanie1\\dane");

    public void print(String message) {
        try (PrintWriter pw = new PrintWriter(new FileWriter(file, false))) {
            pw.println(message);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
