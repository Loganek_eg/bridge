package zadanie1;

public class MySuperService {
    IPrinter printer;

    public MySuperService(IPrinter printer) {
        this.printer = printer;
    }

    public void printData(String message) {
        printer.print(message);
    }
}
