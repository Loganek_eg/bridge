package zadanieSwing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {
    static List<String> lista = new ArrayList<>();


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        lista.add("Tomek");
        lista.add("Marek");
        lista.add("Bartek");
        lista.add("Miłosz");
        lista.add("Weronika");
        lista.add("Dominika");
        lista.add("Daria");
        lista.add("Melchior");

        List<String> listaZnalezonych = new ArrayList<>();


        while (true) {
            System.out.println("Podaj litere na które ma zaczynać się imie");
            String inputLine = sc.nextLine();
            for (String s : lista) {
                if (s.startsWith(inputLine.toUpperCase())) listaZnalezonych.add(s);
            }
            System.out.println(listaZnalezonych);

            listaZnalezonych = lista
                    .stream()
                    .filter(n -> n.startsWith(inputLine.toUpperCase()))
                    .collect(Collectors.toList());
            System.out.println(listaZnalezonych);

        }


    }
}
